import * as Hapi from '@hapi/hapi';

const init = async () => {

  const server: Hapi.Server = new Hapi.Server({
    port: 3001,
    host: 'localhost',
  });

  server.route({
    method: 'GET',
    path: '/',
    handler: (request: Hapi.Request, h: any) => {

      return 'Cats are better!';
    }
  });

  await server.start();
  console.log('Cats are listening - %s', server.info.uri);
};

init();
