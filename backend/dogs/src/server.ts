import * as express from 'express';


const app = express();
const port = 3000;

app.get('/', (req: express.Request, res: express.Response) => res.send('Dogs are better!'));

app.listen(port, () => console.log(`Dogs are listening - http://localhost:${port}!`));
