import './index.scss';

interface Hello {
  hi: string;
  language: string;
}

(function (hello: Hello) {
  alert(`${hello.hi} world (${hello.language})`);
})({
  hi: 'Hello',
  language: 'English'
});